function dispatchAddCartEvent(productName, productID, price, priceID, usageType) {
    var event = new CustomEvent("addCart", {
      detail: {
        "name": productName,
        "price": price,
        "product_id": productID,
        "price_id": priceID,
        "usage_type": usageType,

      }
    });

    document.dispatchEvent(event);
    alert(productName +" has been added to your Cart.");
}
function dispatchDelCartEvent(productName, productID, price, priceID, usageType) {
    var event = new CustomEvent("delCart", {
      detail: {
        "name": productName,
        "price": price,
        "product_id": productID,
        "price_id": priceID,
        "usage_type": usageType,
      }
    });
    document.dispatchEvent(event);
    alert(productName +" has been removed to your Cart.");
}
function checkoutEvent(type, price) {
    var event = new CustomEvent("checkoutCart", {
      detail: {
        "payment_type": type,
        "price": price,
      }
    });
    document.dispatchEvent(event);
}