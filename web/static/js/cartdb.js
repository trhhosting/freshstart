//prefixes of implementation that we want to test
    window.indexedDB = window.indexedDB || window.mozIndexedDB || 
    window.webkitIndexedDB || window.msIndexedDB;
    
    //prefixes of window.IDB objects
    window.IDBTransaction = window.IDBTransaction || 
    window.webkitIDBTransaction || window.msIDBTransaction;
    window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || 
    window.msIDBKeyRange
    
    if (!window.indexedDB) {
    window.alert("Your browser doesn't support a stable version of IndexedDB Upgrade to Chrome or Firefox.")
    }
    
    const cartData = [];
    var db;
    var request = window.indexedDB.open("cart", 1);
    
    request.onerror = function(event) {
    console.log("error: ");
    };
    
    request.onsuccess = function(event) {
    db = request.result;
    console.log("success: "+ db);
    };
    
    request.onupgradeneeded = function(event) {
    var db = event.target.result;
    var objectStore = db.createObjectStore("cart", {keyPath: "id"});
    
    for (var i in cartData) {
        objectStore.add(cartData[i]);
    }
    }
    
    function read() {
    var transaction = db.transaction(["cart"]);
    var objectStore = transaction.objectStore("cart");
    var request = objectStore.get("00-03");
    
    request.onerror = function(event) {
        alert("Unable to retrieve daa from database!");
    };
    
    request.onsuccess = function(event) {
        // Do something with the request.result!
        if(request.result) {
            alert("Name: " + request.result.name + 
                "Age: " + request.result.age + ", Email: " + request.result.email);
        } else {
            alert("Kenny couldn't be found in your database!");
        }
    };
    }
    
    function readAll() {
    var objectStore = db.transaction("cart").objectStore("cart");
    
    objectStore.openCursor().onsuccess = function(event) {
        var cursor = event.target.result;
        
        if (cursor) {
            alert("Name for id " + cursor.key + " is " + cursor.value.name +  
                "Age: " + cursor.value.age + ", Email: " + cursor.value.email);
            cursor.continue();
        } else {
            alert("No more entries!");
        }
    };
    }


    var items = 0;
    function add(productName, productID, price) {
    var request = db.transaction(["cart"], "readwrite")
    .objectStore("cart")
    .add({
        id: items,
        name: productName,
        price: price,
        product_id: productID,
      });
    items++;
    dispatchAddToCartEvent(productName, productID, price);
    request.onsuccess = function(event) {
        alert(productName +" has been added to your Cart.");
    };
    
    request.onerror = function(event) {
        alert("Unable to add data\r\n"+ productName +"is aready exist in your database! ");
    }
    }
    
    function remove() {
    var request = db.transaction(["cart"], "readwrite")
    .objectStore("cart")
    .delete("00-03");
    
    request.onsuccess = function(event) {
        alert("Kenny's entry has been removed from your database.");
    };
    }

    function dispatchAddToCartEvent(productName, productID, price) {
    var event = new CustomEvent("addToCart", {
      detail: {
        "name": productName,
        "price": price,
        "product_id": productID,
      }
    });

    document.dispatchEvent(event);
  }